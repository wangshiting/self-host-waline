# self-host-waline

#### 介绍

自有主机部署waline评论

#### 安装教程

1. 克隆本仓库 ``git clone https://gitee.com/wstee/self-host-waline.git``
2. ``npm i`` 或者 ``yarn`` 安装库
3. 进入项目目录修改``.env``环境变量
4. ``npm start`` 或 ``yarn start``启动项目

[独立部署waline评论系统](https://www.wstee.com/2022/08/22/self-host-waline.html#%E8%AF%84%E8%AE%BA%E6%9C%8D%E5%8A%A1%E7%AB%AF%E5%8F%A3%E4%B8%BA8360-%E4%BD%BF%E7%94%A8nginx%E5%8F%8D%E5%90%91%E4%BB%A3%E7%90%86)